LATEX_FLAGS = -output-format=pdf -output-directory=build

report.pdf : report.tex report.bib
	latex ${LATEX_FLAGS} report.tex; \
	bibtex build/report.aux; \
	latex ${LATEX_FLAGS} report.tex; \
	latex ${LATEX_FLAGS} report.tex
