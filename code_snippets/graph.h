class Graph
{
public:
  struct Connection
  {
    float m_cost;
    uint32_t m_to, m_from;
  };
  struct ConnectionArray
  {
    uint32_t numConnections;
    Connection connections[MAX_CONNECTIONS_PER_NODE];

    ConnectionArray() : numConnections( 0 ) {}
  };

  struct NodeIdHasher
  {
    uint32_t operator() ( uint32_t node ) const
    {
      return node + 1;
    }
  };

public:
  Graph( uint32_t capacity = MAX_NODES );

  void AddConnection( uint32_t to, uint32_t from, float cost );
  void RemoveConnection( uint32_t to, uint32_t from );

  uint32_t GetConnections( Connection *connections, uint32_t size,
                           uint32_t fromNode ) const;

  uint32_t GetNumConnections() const;

  uint32_t GetAllConnections( Connection *connections, uint32_t size ) const;

  uint32_t GetDegree( uint32_t node ) const;

  bool IsConnected( uint32_t node1, uint32_t node2 ) const;

private:
  uint32_t m_numConnections;
  HashMap<ConnectionArray, uint32_t, NodeIdHasher> m_hashMap;
};
