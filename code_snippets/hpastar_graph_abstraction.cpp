typedef std::vector<std::vector<uint32_t>> ClusterNodesArray_t;

struct HPAStarContext
{
  const NodeGrid *grid;
  const Graph *graph;
  NodeGrid *abstractGrid;
  Graph *abstractGraph;
  AStarContext *astarContext;
  uint32_t clusterSize, mapSize, clustersPerDim, clustersPerMap,
    maxLengthForSingleTransition;
  ClusterNodesArray_t clusterNodes;
  SearchSpaceSet searchSpace;
  PathCache pathCache;
};

HPAStarContext *AbstractGraphHPAStar( const Graph &graph,
                                      const NodeGrid &grid,
                                      Graph *abstractGraph,
                                      NodeGrid *abstractGrid,
                                      AStarContext *astarContext )
{
  PROFILE_SCOPE( "HPA* Graph Abstraction" );
  HPAStarContext *context = new HPAStarContext;
  context->clusterSize = 16;
  context->mapSize = 512;
  context->maxLengthForSingleTransition = 6;
  context->clustersPerDim = context->mapSize / context->clusterSize;
  context->clustersPerMap = context->clustersPerDim *
                            context->clustersPerDim;
  context->searchSpace = SearchSpaceSet( grid.GetNumNodes() );
  context->grid = &grid;
  context->graph = &graph;
  context->abstractGraph = abstractGraph;
  context->abstractGrid = abstractGrid;
  context->astarContext = astarContext;

  context->clusterNodes.resize( context->clustersPerMap );

  CreateAbstractNodes( context );
  CreateInterEdges( context );
  CreateIntraEdges( context, astarContext );

  return context;
}

void CreateIntraEdges( HPAStarContext *context,
                       AStarContext *astarContext )
{
  SearchSpaceSet searchSpace( context->clusterSize *
                              context->clusterSize * 5 );
  std::vector<uint32_t> startingNodes;
  uint32_t pathNodes[32]; // 2x clusterSize
  for ( uint32_t clusterIdx = 0; clusterIdx < context->clustersPerMap;
        ++clusterIdx )
  {
    searchSpace.clear();
    startingNodes.clear();
    AddGridTileNodesToSearchSpace( context, &searchSpace, clusterIdx );
    AddClusterNodesToArray( context, &startingNodes, clusterIdx );

    for ( auto startingNode : startingNodes )
    {
      for ( auto goalNode : startingNodes )
      {
        if ( startingNode != goalNode )
        {
          int length = AStar( *context->graph, *context->grid,
                              pathNodes, 32, startingNode, goalNode,
                              context->astarContext, &searchSpace );
          if ( length > 0 )
          {
            context->pathCache.AddPath( startingNode, goalNode,
                                        pathNodes, length );
            uint32_t node1, node2;
            auto startResult =
              context->grid->GetNodePosition( startingNode );
            auto goalResult =
              context->grid->GetNodePosition( goalNode );
            context->abstractGrid->GetNodeAtPosition(
                startResult.position, node1 );
            context->abstractGrid->GetNodeAtPosition(
                goalResult.position, &node2 );
            context->abstractGraph->AddConnection( node2, node1,
                                                   (float)length );
            context->abstractGraph->AddConnection( node1, node2,
                                                   (float)length );
          }
        }
      }
    }
  }
}
