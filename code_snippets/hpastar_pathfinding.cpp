int HPAStar( HPAStarContext *context, uint32_t *pathNodes,
             uint32_t maxPathSize, uint32_t startNode,
             uint32_t goalNode )
{
  auto abstractStartResult = AddNodeToAbstractGraph( context,
                                                     startNode );
  auto abstractGoalResult = AddNodeToAbstractGraph( context,
                                                    goalNode );
  uint32_t abstractPath[4096];
  int abstractLength =
    AStar( *context->abstractGraph, *context->abstractGrid,
           abstractPath, 4096, abstractStartResult.node,
           abstractGoalResult.node, context->astarContext,
           &context->searchSpace );
  int length = 0;
  for ( int i = 0; i < abstractLength - 1; ++i )
  {
    uint32_t tempPath[32]; // Based on cluster size.
    auto result1 =
      context->abstractGrid->GetNodePosition( abstractPath[i] );
    auto result2 =
      context->abstractGrid->GetNodePosition( abstractPath[i+1] );

    uint32_t a, b;
    context->grid->GetNodeAtPosition( result1.position, &a );
    context->grid->GetNodeAtPosition( result2.position, &b );
    int n = 0;
    if ( context->graph->IsConnected( a, b ) )
    {
      n = 2;
      tempPath[0] = a;
      tempPath[1] = b;
    }
    else
    {
      n = context->pathCache.GetPath( a, b, tempPath, 32 );
    }

    if ( !CopyPathNodes( length, pathNodes, maxPathSize,
                         tempPath, n ) )
    {
      break;
    }
  }
  RemoveNodeFromAbstractGraph( context, abstractStartResult.node );
  RemoveNodeFromAbstractGraph( context, abstractGoalResult.node );

  return length;
}
