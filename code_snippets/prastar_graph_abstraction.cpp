void AbstractCliques( const Graph &graph, const NodeGrid &nodeGrid,
                      NodeGrid *abstractGrid,
                      GraphAbstractionTree *tree,
                      BinarySet *availableNodes, uint32_t level )
{
  for ( uint32_t cliqueSize = 4; cliqueSize >= 2; --cliqueSize )
  {
    for ( uint32_t node = 0; node < nodeGrid.GetNumNodes(); ++node )
    {
      if ( availableNodes->contains( node ) )
      {
        uint32_t clique[4];
        if ( FindClique( node, graph, clique, cliqueSize,
                         *availableNodes ) )
        {
          auto positionResult = nodeGrid.GetNodePosition( node );
          assert( positionResult.isValid );
          auto abstractNode =
            abstractGrid->AddNode( positionResult.position );

          for ( uint32_t i = 0; i < cliqueSize; ++i )
          {
            tree->AddParentNode( clique[i], abstractNode, level );
            availableNodes->remove( clique[i] );
          }
        }
      }
    }
  }
}

void AbstractRemainingNodes( const Graph &graph, const NodeGrid &nodeGrid,
                             NodeGrid *abstractGrid, GraphAbstractionTree *tree,
                             const BinarySet availableNodes, uint32_t level )
{
  for ( uint32_t node = 0; node < nodeGrid.GetNumNodes(); ++node )
  {
    if ( availableNodes.contains( node ) )
    {
      Graph::Connection connections[2];
      auto n = graph.GetConnections( connections, 2, node );
      if ( n == 1 )
      {
        auto parent = tree->GetParent( connections[0].m_to, level );
        tree->AddParentNode( node, parent, level );
      }
      else
      {
        auto positionResult = nodeGrid.GetNodePosition( node );
        assert( positionResult.isValid );
        auto abstractNode = abstractGrid->AddNode( positionResult.position );
        tree->AddParentNode( node, abstractNode, level );
      }
    }
  }
}

void CalculateAbstractNodePositions( const NodeGrid &nodeGrid,
                                     NodeGrid *abstractGrid,
                                     GraphAbstractionTree *tree,
                                     uint32_t level )
{
  uint32_t children[MAX_NODES_PER_ABSTRACT_NODE];
  for ( uint32_t abstractNode = 0; abstractNode < abstractGrid->GetNumNodes();
        ++abstractNode )
  {
    auto n = tree->GetChildren( abstractNode, level + 1, children,
                                MAX_NODES_PER_ABSTRACT_NODE );
    NodePosition p = {};
    for ( uint32_t childIdx = 0; childIdx < n; ++childIdx )
    {
      auto positionResult = nodeGrid.GetNodePosition( children[childIdx] );
      assert( positionResult.isValid );
      p.x += positionResult.position.x;
      p.y += positionResult.position.y;
    }
    p.x /= n;
    p.y /= n;
    abstractGrid->SetNodePosition( abstractNode, p );
  }
}

void CreateAbstractNodeConnections(
  const Graph &graph, const NodeGrid &nodeGrid,
  const NodeGrid &abstractGrid, Graph *abstractGraph,
  const GraphAbstractionTree &tree, uint32_t level )
{
  for ( uint32_t i = 0; i < nodeGrid.GetNumNodes(); ++i )
  {
    Graph::Connection connections[MAX_CONNECTIONS_PER_NODE];
    auto n = graph.GetConnections( connections,
                                   MAX_CONNECTIONS_PER_NODE, i );
    auto parent1 = tree.GetParent( i, level );
    for ( uint32_t j = 0; j < n; ++j )
    {
      auto parent2 = tree.GetParent( connections[j].m_to, level );
      if ( parent1 != parent2 )
      {
        auto r1 = abstractGrid.GetNodePosition( parent1 );
        auto r2 = abstractGrid.GetNodePosition( parent2 );
        assert( r1.isValid && r2.isValid );
        auto d = CalculateEuclideanDistance( r1.position,
                                             r2.position );
        abstractGraph->AddConnection( parent2, parent1, d );
      }
    }
  }
}

void AbstractGraph( const Graph &graph, const NodeGrid &nodeGrid,
                    Graph *abstractGraph, NodeGrid *abstractGrid,
                    GraphAbstractionTree *tree, uint32_t level )
{
  BinarySet availableNodes( nodeGrid.GetNumNodes() );
  for ( uint32_t node = 0; node < nodeGrid.GetNumNodes(); ++node )
  {
    availableNodes.add( node );
  }

  tree->InitializeLevel( level + 1, nodeGrid.GetNumNodes() );

  AbstractCliques( graph, nodeGrid, abstractGrid, tree,
                   &availableNodes, level );

  AbstractRemainingNodes( graph, nodeGrid, abstractGrid, tree,
                          availableNodes, level );

  CalculateAbstractNodePositions( nodeGrid, abstractGrid, tree,
                                  level );

  CreateAbstractNodeConnections( graph, nodeGrid, *abstractGrid,
                                 abstractGraph, *tree, level );
}

inline bool IsNodeConnectedTo( const Graph &graph, uint32_t node,
                               uint32_t *otherNodes, uint32_t numOtherNodes )
{
  Graph::Connection connections[8];
  uint32_t n = graph.GetConnections( connections, 8, node );
  if ( n < numOtherNodes )
  {
    return false;
  }
  for ( uint32_t i = 0; i < numOtherNodes; ++i )
  {
    bool found = false;
    for ( uint32_t j = 0; j < n; ++j )
    {
      if ( connections[j].m_to == otherNodes[i] )
      {
        found = true;
        break;
      }
    }
    if ( !found )
    {
      return false;
    }
  }
  return true;
}

bool FindClique( uint32_t node, const Graph &graph, uint32_t *clique,
                 uint32_t cliqueSize, const BinarySet &availableNodes )
{
  Graph::Connection connections[8];
  auto n = graph.GetConnections( connections, 8, node );
  clique[0] = node;
  if ( ( n >= 1 ) && ( cliqueSize == 2 ) )
  {
    if ( availableNodes.contains( connections[0].m_to ) )
    {
      clique[1] = connections[0].m_to;
      return true;
    }
  }

  uint32_t otherNodes[8];
  uint32_t numNodes = 0;
  for ( uint32_t i = 0; i < n; ++i )
  {
    if ( availableNodes.contains( connections[i].m_to ) )
    {
      otherNodes[numNodes++] = connections[i].m_to;
    }
  }

  uint32_t connectedNodes[8];
  connectedNodes[0] = node;
  uint32_t numConnectedNodes = 1;
  for ( uint32_t i = 0; i < numNodes; ++i )
  {
    if ( IsNodeConnectedTo( graph, otherNodes[i], connectedNodes,
                            numConnectedNodes ) )
    {
      connectedNodes[numConnectedNodes++] = otherNodes[i];
      if ( numConnectedNodes == cliqueSize )
      {
        std::copy_n( connectedNodes, cliqueSize, clique );
        return true;
      }
    }
  }
  return false;
}
