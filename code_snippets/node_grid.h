class NodeGrid
{
public:
  NodeGrid( uint32_t capacity = MAX_NODES );
  uint32_t AddNode( NodePosition p );
  GetNodePositionResult GetNodePosition( uint32_t node ) const;
  bool GetNodeAtPosition( NodePosition p, uint32_t *node ) const;
  bool GetNodeAtPosition( uint32_t x, uint32_t y, uint32_t *node ) const;

  void SetNodePosition( NodePosition p, uint32_t node ) const;

  void RemoveNode( uint32_t node );

  inline uint32_t GetNumNodes() const { return m_nodePositions.size(); }
  inline uint32_t GetXMin() const { return m_xMin; }
  inline uint32_t GetYMin() const { return m_yMin; }
  inline uint32_t GetXMax() const { return m_xMax; }
  inline uint32_t GetYMax() const { return m_yMax; }

private:
  std::vector<NodePosition> m_nodePositions;
  HashMap<uint32_t, NodePosition, NodePositionHasher> m_hashMap;
  uint32_t m_xMin, m_yMin, m_xMax, m_yMax;
};
