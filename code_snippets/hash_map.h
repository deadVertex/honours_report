template<typename T>
struct NullHasher
{
  uint32_t operator() ( const T &value ) const
  {
    return (uint32_t)value;
  }
};

template<typename T, typename K, typename H = NullHasher<K>>
class HashMap
{
public:
  HashMap( std::size_t capacity );

  void add( K key, const T &value );
  T* retrieve( K key );
  const T* retrieve( K key ) const;
  void clear();
  bool empty() const { return m_size == 0; }
  void remove( K key );
  uint32_t get_next_index( uint32_t idx ) const;

private:
  std::vector<uint32_t> m_keys;
  std::vector<T> m_values;
  std::size_t m_size;
  H m_hasher;
};
