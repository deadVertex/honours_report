class GraphAbstractionTree
{
public:
  GraphAbstractionTree();
  void InitializeLevel( uint32_t level, uint32_t size );
  void CreateNode( uint32_t node, uint32_t level );
  void AddParentNode( uint32_t child, uint32_t parent, uint32_t childLevel );
  uint32_t GetParent( uint32_t node, uint32_t level ) const;
  uint32_t GetChildren( uint32_t parent, uint32_t childLevel,
                        uint32_t *children, uint32_t size ) const;

private:
  struct ChildNode
  {
    uint32_t node;
    ChildNode *next;
  };
  struct HashMapEntry
  {
    uint32_t node, parent;
    ChildNode *childrenListHead;
  };

private:
  HashMap<HashMapEntry, uint32_t, Graph::NodeIdHasher>
    m_hashMaps[MAX_ABSTRACTION_LEVELS];
};
