template< typename T, uint32_t Size, typename Compare >
class Heap
{
public:
  Heap();

  void Push( const T &val );
  T Pop();

  unsigned int GetSize() const { return m_current; }
  bool IsEmpty() const { return GetSize() == 0; }

  void Clear() { m_current = 0; }

  bool Replace( const T &val, T *replaced = NULL );

private:
  std::array< T, Size > m_array;
  uint32_t m_current;
  Compare m_compare;
};
