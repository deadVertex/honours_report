FindStartingLevel( startNode, goalNode, numLevels, startingNodes[],
                   goalNodes[], tree )
{
  startingNodes[0] = startNode;
  goalNodes[0] = goalNode;
  for ( childLevel = 0; childLevel < numLevels; ++childLevel )
  {
    parentLevel = childLevel + 1;
    startNode = tree.GetParent( startNode, childLevel );
    startingNodes[parentLevel] = startNode;
    goalNode = tree.GetParent( goalNode, childLevel );
    goalNodes[parentLevel] = goalNode;
    if ( startNode == goalNode )
    {
      return childLevel;
    }
  }
  return -1; // No path exists.
}

