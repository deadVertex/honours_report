AddChildrenToSet( parent, level, searchSpace, tree )
{
  children = tree.GetChildren( parent, level - 1 );
  for ( i = 0; i < children.size; ++i )
  {
    searchSpace.add( children[i] );
  }
}

PRAStar( graphLevels[], gridLevels[], startNode, goalNode, tree,
         numLevels )
{
  startingNodes[], goalNodes[], searchSpace[];
  startingLevel = FindStartingLevel( startNode, goalNode, numLevels,
                                     startingNodes, goalNodes, tree );
  if ( startingLevel == -1 )
  {
    return; // No path exists.
  }

  parent = tree.GetParent( startingNodes[startingLevel],
                           startingLevel );

  AddChildrenToSet( parent, startingLevel + 1, searchSpace, tree );

  for ( currentLevel = startingLevel;
        currentLevel >= 0;
        --currentLevel )
  {
    path = AStar( graphLevels[currentLevel], gridLevel[currentLevel],
                  startingNodes[currentLevel], goalNodes[currentLevel],
                  searchSpace );

    searchSpace.clear();
    if ( currentLevel > 0 )
    {
      for ( i = 0; i < path.length; ++i )
      {
        AddChildrenToSet( path.nodes[i], currentLevel, searchSpace,
                          tree );
      }
    }
  }
  return path;
}
